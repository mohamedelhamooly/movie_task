//
//  Enums.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import Foundation
public enum status {
    case isloading(Bool)
    case errorMessage(String)
    case noData(String)
}
public enum operationType {
    case loadingFirst
    case loadMore
}

enum APIError: String, Error {
    case noNetwork = "No Network"
    case serverOverload = "Server is overloaded"
    case permissionDenied = "You don't have permission"
}
