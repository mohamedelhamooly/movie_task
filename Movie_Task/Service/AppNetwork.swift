//
//  AppNetwork.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import Foundation
import Moya
import RxSwift

let loggerPlugin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)

let manager = Manager()

let endpointClosure = { (target: AppNetwork) -> Endpoint in
    let url = target.baseURL.appendingPathComponent(target.path).absoluteString
    
    let endpoint: Endpoint = Endpoint(url: url, sampleResponseClosure: { () -> EndpointSampleResponse in
        return .networkResponse(200, target.sampleData)
    }, method: target.method, task: target.task,httpHeaderFields: target.headers)
    
    var httpHeaders: [String : String] = [:]
    
    if target.dataParameters != nil {
        httpHeaders["Content-Type"] = "application/json"
    }
   
    switch target.requiredAuthentication {
    case .token:
        httpHeaders["Authorization"] = "Bearer "  + AppConfiguration.token
    case .none:
        break
    }
  
    return endpoint.adding(newHTTPHeaderFields: httpHeaders)
}

let provider = MoyaProvider<AppNetwork>(endpointClosure: endpointClosure, plugins: [loggerPlugin])

// MARK: - Provider support

extension String {
    var urlEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

enum AppNetwork {
    case discover(page:Int)
    case search(page: Int,query:String)
}

extension AppNetwork: TargetType {
    
    var headers: [String : String]? {
        return nil
    }
    
    var baseURL : URL {
        return URL(string: AppConfiguration.apiBaseURL)!
    }
    
    var path: String {
        switch self {
        case .discover:
            return "discover/movie"
        case .search:
            return "search/movie"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .discover , .search:
            return .get
        }
    }
    
    var dataParameters: Data? {
        return nil
    }
    
    var parameters: [String: Any]? {
        switch self {
            
        case .discover(let page):
            return ["page": page]
            
        case .search(let page,let query):
            return ["page": page,"query":query]
            

        }
    }
    
    
    var task: Task {
        let parameterEncoding: ParameterEncoding
        switch self.method {
        case .get:
            parameterEncoding = URLEncoding()
        default:
            parameterEncoding = JSONEncoding()
        }
        if let dataParameters = dataParameters {
            return Task.requestData(dataParameters)
        } else {
            return Task.requestParameters(parameters: parameters ?? [:], encoding: parameterEncoding)
        }
    }
    
    var sampleData: Data {
        switch self {
        // need to complete cases ..
        default:
            return "{}".data(using:String.Encoding.utf8)!
        }
    }
    
    fileprivate var requiredAuthentication: AuthenticationType {
        switch self {
        case .discover,.search:
            return .token
        }
    }
}

func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

private enum AuthenticationType {
    // Provides token
    case token
    // No authentication
    case none
}
