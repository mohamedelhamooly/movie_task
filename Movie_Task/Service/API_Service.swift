//
//  APIService.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//


import Foundation
import Moya
import RxSwift

class NetworkService: NetworkServiceProtocol {
    static let shared = NetworkService()
    func request<T>(target: AppNetwork, mapTo type: T.Type) -> Observable<T> where T : Decodable {
        return request(target: target)
             .map { (response: Response) -> T in
                 return try response.map(T.self)
             }
    }
    
    private func request(target: AppNetwork) -> Observable<Response>{
        return provider.rx.request(target)
            .asObservable()
            .map { try $0.filterSuccessfulStatusAndRedirectCodes() }
            .catchError { error -> Observable<Response> in
                let mappedError = self.mapError(error)
                return .error(mappedError)
            }

    }
    
    private func mapError(_ error: Error) -> Error {
        switch error {
        case MoyaError.statusCode(let response):
            let Error = (try? response.map(MetaResponseError.self))
            return NSError (domain: Error?.status_message ?? "", code: response.response!.statusCode, userInfo: nil)
        case MoyaError.underlying(_,_):
            return NSError (domain: "You Are Not Connected To the Internet", code: 404, userInfo: nil)
        default:
            return error
        }
    }


}


