//
//  API_Protocol.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
import Moya
import RxSwift

import Foundation

protocol NetworkServiceProtocol {
    func request<T: Decodable>(target: AppNetwork, mapTo type: T.Type) -> Observable<T>
}
