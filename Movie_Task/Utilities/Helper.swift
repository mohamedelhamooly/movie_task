//
//  Helper.swift
//  samaApp
//
//  Created by Mohamed Elhamoly on 2/17/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//


import Foundation
import SDWebImage
import Alamofire

struct Helper {
    static let instance = Helper()
    
    func configureImage(_ imageString: String,_ image: UIImageView,_ placeholder: String) {
        let imageView = image
        let image_Url  = AppConfiguration.imageBaseURL  + imageString
        let url = URL(string:image_Url)
        imageView.sd_setImage(with: url, placeholderImage: UIImage(named: placeholder))
    }
    func saveArrayStringValue(key:String,value:[String]) {
        UserDefaults.standard.set(value, forKey: key)
        
    }
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    func getArrayStringValue(key:String) -> [String] {
        if(isKeyPresentInUserDefaults(key: key)){
            return  UserDefaults.standard.stringArray(forKey:key) ?? [String]()
        }
        return []
    }
    @available(iOS 10.0, *)
    func openUrl(link : String){
        if let url = URL(string: link) {
            UIApplication.shared.open(url)
        }

    }
}
