//
//  AppConfigurations.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
//

import Foundation

struct AppConfiguration {
    static var apiBaseURL: String {
      return "https://api.themoviedb.org/3/"
    }
    static var imageBaseURL: String {
      return "https://image.tmdb.org/t/p/w500"
    }

    static var token: String {
        return "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlMWI0YTExZDM5NDljZGI2ZThiNTAwMDMwNGFlZmUxMiIsInN1YiI6IjVjODc1N2MxMGUwYTI2MjdmZDViNTMxZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.JVFxapjsVDR7aaSYsgqTvLxRi7BjZg839Bjp6W4B0Jk"
    }
}
