//
//  MoviesCell.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit

class MoviesCell: UITableViewCell {
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var lblMovie_name: UILabel!
    @IBOutlet weak var lblRelease_date: UILabel!
    @IBOutlet weak var txtoverview: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(element:Results)  {
    
        self.lblMovie_name.text = element.original_title ?? ""
        self.lblRelease_date.text = element.release_date ?? ""
        self.txtoverview.text = element.overview ?? ""
        Helper.instance.configureImage(element.poster_path ?? "", imgThumbnail, "backGroundNew-1")
     }
}
