//
//  ViewController.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit

class Home: UIViewController {
    
    
    @IBOutlet weak var discoverViewController: UIView!
    @IBOutlet weak var searchMoviesViewController: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTabSegment (segment :UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            showDiscoverViewController ()
            
        }
        else if segment.selectedSegmentIndex == 1 {
            showdSearchMoviesViewController ()
            
        }
    }
    
    func showDiscoverViewController ()  {
        UIView.animate(withDuration: 0.5, animations: {
            self.discoverViewController.alpha = 1
            self.searchMoviesViewController.alpha = 0
            self.discoverViewController.isHidden = false
            self.searchMoviesViewController.isHidden = true
            self.searchMoviesViewController.endEditing(true)
            
        })

    }
    func showdSearchMoviesViewController ()  {
          UIView.animate(withDuration: 0.5, animations: {
              self.discoverViewController.alpha = 0
              self.discoverViewController.isHidden = true
              self.searchMoviesViewController.isHidden = false
              self.searchMoviesViewController.alpha = 1
          })

      }
}

