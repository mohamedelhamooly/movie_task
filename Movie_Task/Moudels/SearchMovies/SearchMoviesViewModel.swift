//
//  SearchMoviesViewModel.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
import RxSwift
import RxCocoa
import Foundation
class SearchMoviesViewModel {
    let networkService: NetworkServiceProtocol
    
    var lstSearchResult = BehaviorRelay<[Results]>(value: [])
    
    lazy   var bag:DisposeBag = {
        return DisposeBag()
    }()
    
    lazy   var requestStatus : PublishSubject<status> = {
        return PublishSubject()
    }()
    
    init( networkService: NetworkServiceProtocol = NetworkService.shared) {
        self.networkService = networkService
    }
    
    func initFetch(type : operationType,pageNumber:Int,searchText:String) {
        self.requestStatus.onNext(.isloading(true))
        
        self.networkService.request(target:.search(page:pageNumber, query: searchText), mapTo:Movies_response.self)
            .subscribe(onNext: {[weak self] (jsonResponse) in
                if jsonResponse.results != nil {
                    self?.appendData(data: jsonResponse.results!,type: type)
                }
                else {
                    self?.requestStatus.onNext(.noData("No More Data"))
                }
                
                self?.requestStatus.onNext(.isloading(false))
                }
                , onError: { [weak self] (error) in
                    
                    let ErrorModel = error as NSError
                    self?.requestStatus.onNext(.errorMessage(ErrorModel.domain))
                    self?.requestStatus.onNext(.isloading(false))
                    
            }).disposed(by: bag)
    }
    
    func appendData(data: [Results],type: operationType)  {
        switch type {
        case .loadingFirst:
            if data.count != 0 {
                lstSearchResult.accept(data)
                saveRecent(data: data)
            }
        case .loadMore:
            if data.count != 0 {
                lstSearchResult.accept((self.lstSearchResult.value) + data)
                saveRecent(data: lstSearchResult.value)
            }
            else {
                self.requestStatus.onNext(.noData("No More Data"))
            }
        }
    }
    
    func saveRecent(data: [Results])  {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(data)
        UserDefaults.standard.set((String(data: orderJsonData, encoding: .utf8)!), forKey: "successful_searches")
    }
    
    func getRecentSearchs()  {
        let fileUrl =  UserDefaults.standard.string(forKey: "successful_searches")
        print(fileUrl!)
        let json = Data(fileUrl!.utf8)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601 //This is irrelevant though because the date is returned as a string.
        
        do{
            let beer = try decoder.decode([Results].self, from: json)
            self.lstSearchResult.accept(beer)
        }catch let error{
            print ("error is \(error)")
        }
    }
}
