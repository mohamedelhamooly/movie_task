//
//  SearchMovies.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchMovies: BaseViewController,UITableViewDelegate {
    @IBOutlet weak var tvMovies: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let searchText = Variable<String?>(nil)
    private var pageNumber = 1
    private var cellIdentifier = "MoviesCell"
    
    private lazy var bg: DisposeBag = {
        return DisposeBag()
    } ()
    
    private lazy var objSearchMoviesViewModel: SearchMoviesViewModel = {
        return SearchMoviesViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        searchSetup()
        
    }
    
    func setup()  {
        tvMovies.register(UINib(nibName: "MoviesCell", bundle: nil), forCellReuseIdentifier: "MoviesCell")
        self.tvMovies.delegate =  self
        checkStatus()
        bindMoviesData()
    }
    
    func bindMoviesData()  {
        objSearchMoviesViewModel.lstSearchResult.asObservable().bind(to: tvMovies.rx.items(cellIdentifier:
            cellIdentifier, cellType: MoviesCell.self )) {(row, element, cell) in
                cell.setData(element: element)
        }.disposed(by: bg)
        
        
    }
    
    func searchSetup() {
        searchBar.rx.text.asDriver()
            .drive(searchText)
            .disposed(by: bg)
        searchText.asObservable().subscribe(onNext: { [weak self] (text) in
            if text != "" {
                self?.objSearchMoviesViewModel.initFetch(type: .loadingFirst, pageNumber: 1, searchText: text!)
            }
            else {
                self?.pageNumber = 1
                self?.objSearchMoviesViewModel.getRecentSearchs()
            }
            
        })
            .disposed(by: bg)
        
        
    }
    
    func checkStatus()  {
        objSearchMoviesViewModel
            .requestStatus
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](status) in
                switch status {
                case .isloading(let isloading):
                    if isloading
                    {
                        self?.showLoading()
                    }
                    else
                    {
                        self?.hideLoading()
                        
                    }
                    
                case .errorMessage(let message):
                    self?.view.makeToast(message)
                case .noData( let message ):
                    self?.view.makeToast(message)
                    break
                    
                }
                
            }).disposed(by:bg)
    }
    
    
}
// MARK: Handel Load more
extension SearchMovies {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == tvMovies{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if searchBar.text != "" {
                    pageNumber  +=  1
                    objSearchMoviesViewModel.initFetch(type: .loadMore,pageNumber: pageNumber, searchText: searchBar.text!)
                }
                
            }
        }
        
    }
}
