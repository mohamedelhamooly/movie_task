//
//  DiscoverMovies.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class DiscoverMovies: BaseViewController,UITableViewDelegate {
    @IBOutlet weak var tvMovies: UITableView!
    private var pageNumber = 1
    private var cellIdentifier = "MoviesCell"
    
    private lazy var bg: DisposeBag = {
        return DisposeBag()
    } ()
    private lazy var objDiscoverMoviesViewModel: DiscoverMoviesViewModel = {
        return DiscoverMoviesViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        setup()
    }
    func setup()  {
        tvMovies.register(UINib(nibName: "MoviesCell", bundle: nil), forCellReuseIdentifier: "MoviesCell")
        self.tvMovies.delegate =  self
        objDiscoverMoviesViewModel.initFetch( type: .loadingFirst , pageNumber: self.pageNumber)
        checkStatus()
        bindMoviesData()
        
    }
    func bindMoviesData()  {
        objDiscoverMoviesViewModel.lstMovies.asObservable().bind(to: tvMovies.rx.items(cellIdentifier:
            cellIdentifier, cellType: MoviesCell.self )) {(row, element, cell) in
                cell.setData(element: element)
        }.disposed(by: bg)
        
    }
    func checkStatus()  {
        objDiscoverMoviesViewModel
            .requestStatus
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](status) in
                switch status {
                case .isloading(let isloading):
                    if isloading
                    {
                        self?.showLoading()
                    }
                    else
                    {
                        self?.hideLoading()
                        
                    }
                    
                case .errorMessage(let message):
                    self?.view.makeToast(message)
                case .noData( let message ):
                    self?.view.makeToast(message)
                    break
                    
                }
                
            }).disposed(by:bg)
    }

}
// MARK: Handel Load more
extension DiscoverMovies {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == tvMovies{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                pageNumber  +=  1
                objDiscoverMoviesViewModel.initFetch(type: .loadMore,pageNumber: pageNumber)
                
            }
        }
        
    }
}
