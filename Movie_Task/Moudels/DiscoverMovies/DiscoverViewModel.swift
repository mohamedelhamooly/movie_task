//
//  DiscoverMvvm.swift
//  Movie_Task
//
//  Created by Mohamed Elhamoly on 10/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
import RxSwift
import RxCocoa
import Foundation
class DiscoverMoviesViewModel {
    let networkService: NetworkServiceProtocol
    var lstMovies = BehaviorRelay<[Results]>(value: [])
    lazy   var bag:DisposeBag = {
        return DisposeBag()
    }()
    lazy   var requestStatus : PublishSubject<status> = {
        return PublishSubject()
    }()
    
    init( networkService: NetworkServiceProtocol = NetworkService.shared) {
        self.networkService = networkService
    }
    
    func initFetch(type : operationType,pageNumber:Int) {
        self.requestStatus.onNext(.isloading(true))
        
        self.networkService.request(target: .discover(page:pageNumber), mapTo:Movies_response.self)
        .subscribe(onNext: {[weak self] (jsonResponse) in
            if jsonResponse.results != nil {
                self?.appendData(data: jsonResponse.results!,type: type)
            }
            else {
                self?.requestStatus.onNext(.noData("No More Data"))
            }
            
            self?.requestStatus.onNext(.isloading(false))
            }
            , onError: { [weak self] (error) in
                let ErrorModel = error as NSError
                self?.requestStatus.onNext(.errorMessage(ErrorModel.domain))
                self?.requestStatus.onNext(.isloading(false))
            }
        ).disposed(by: bag)
        
    }
    func appendData(data: [Results],type: operationType)  {
        switch type {
        case .loadingFirst:
            if data.count != 0 {
                self.lstMovies.accept(data)
            }
        case .loadMore:
            if data.count != 0 {
                self.lstMovies.accept((self.lstMovies.value) + data)
            }
            else {
                self.requestStatus.onNext(.noData("No More Data"))
            }
        }
    }
    
}
